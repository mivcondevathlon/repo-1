package net.mivcon.devathlon;

import java.util.ArrayList;
import java.util.List;

import net.mivcon.devathlon.api.MySQL;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author ilouHD
 * */
public class Main extends JavaPlugin {
	
	private static Main instance;
	public static Main getInstance() { return instance; }
	public Main() { Main.instance = this; }
	
	/*
	 * Data
	 * */
	
	private MySQL mysql;
	
	private String prefix;
	
	private List<Location> spawns;
	private List<Player> iPlayers;
	
	private Location lobbyLocation;
	
	/*
	 * Enable
	 * Disable
	 * */
	
	@Override
	public void onEnable() {
		this.mysql = new MySQL();
		
		this.prefix = "";
		
		this.spawns = new ArrayList<>();
		this.iPlayers = new ArrayList<>();
		
		this.registerEvents();
		this.registerCommands();
	}
	
	@Override
	public void onDisable() {
		
	}
	
	/*
	 * Events
	 * Commands
	 * */
	
	private void registerEvents() {
		
	}
	
	private void registerCommands() {
		
	}
	
	/*
	 * Getter
	 * Setter
	 * */
	
	public MySQL getMySQL() {
		return this.mysql;
	}

	public String getPrefix() {
		return this.prefix;
	}
	
	public List<Location> getSpawns() {
		return this.spawns;
	}
	
	public List<Player> getIngamePlayers() {
		return this.iPlayers;
	}
	
	public void addIngamePlayer(Player player) {
		this.iPlayers.add(player);
	}
	
	public void removeIngamePlayer(Player player) {
		if(this.iPlayers.contains(player)) {
			this.iPlayers.remove(player);
		}
	}
	
	public Location getLobbyLocation() {
		return this.lobbyLocation;
	}

}
