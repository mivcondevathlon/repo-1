package me.ilouhd.creepplays.devathlon;

import java.io.File;

import me.ilouhd.creepplays.devathlon.api.MySQL;
import me.ilouhd.creepplays.devathlon.listener.EntityDeathListener;
import me.ilouhd.creepplays.devathlon.listener.PlayerJoinListener;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	public String prefix = "Prefix ";
	
	private static MySQL mysql;
	public static MySQL getMySQL() { return mysql; }
	
	public Main() { instance = this; }
	private static Main instance;
	public static Main getInstance() { return instance; }
	
	@Override
	public void onEnable() {
		mysql = new MySQL();
		getCommand("test").setExecutor(new TestCommand());
		
		this.getServer().getPluginManager().registerEvents(new EntityDeathListener(), this);
		this.getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
	}
	
	@Override
	public void onDisable() {
		
	}
	
	@Override
	public File getFile() {
		return new File("plugins/Mivcon/", "mysql.json");
	}
	
}
