package me.ilouhd.creepplays.devathlon.manager;

public enum GameState {
	
	LOBBY, INGAME, DEATHMATCH, SHUTDOWN;

}
