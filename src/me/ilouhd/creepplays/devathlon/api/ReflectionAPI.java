package me.ilouhd.creepplays.devathlon.api;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import org.bukkit.Bukkit;

/**
 * 
 * @author CreepPlays
 *
 */
public class ReflectionAPI {
	
	private static String version;
	private static String nmspackage;
	
	static {
		version = Bukkit.getServer().getClass().getPackage().getName().split(".")[3];
		nmspackage = "net.minecraft.server." + getVersion();
	}
	
	/**
	 * Get version of Spigot API
	 * @return The version (e.g. 1_8_R3)
	 */
	public static String getVersion() {
		return version;
	}
	
	/**
	 * Get NMS package
	 * @return The package name (e.g. net.minecraft.server.1_8_R3)
	 */
	public static String getNMSPackage() {
		return nmspackage;
	}
	
	/**
	 * Get CraftBukkit package
	 * @return The package name (e.g. org.craftbukkit.1_8_R3)
	 */
	public static String getCBPackage() {
		return nmspackage;
	}
	
	/**
	 * Get class in the NMS package
	 * @param classname The class name
	 * @return The class
	 */
	public static Class<?> getNMSClass(String classname) {
		Class<?> clazz = null;
		try {
			clazz = Class.forName(getNMSPackage() + "." + classname);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return clazz;
	}
	
	/**
	 * Get class in the CraftBukkit package
	 * @param classname The class name
	 * @return The class
	 */
	public static Class<?> getCBClass(String classname) {
		Class<?> clazz = null;
		try {
			clazz = Class.forName(getCBPackage() + "." + classname);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return clazz;
	}
	
	/**
	 * A useful util to manage reflection easily
	 * @author Alex
	 */
	public static class ReflectionObject {
		
		private HashMap<String, Object> fields = new HashMap<>();
		
		private HashMap<String, Object[]> methods = new HashMap<>();
		
		private Object object;
		
		/**
		 * Constructor of the ReflectionObject
		 * @param o The object to affect
		 */
		public ReflectionObject(Object o) {
			this.object = o;
		}
		
		/**
		 * Set a field
		 * @param field The field
		 * @param obj The data
		 * @return The reflection object
		 */
		public ReflectionObject set(String field, Object obj) {
			fields.put(field, obj);
			
			return this;
		}
		
		/**
		 * Get data of field
		 * @param field The field
		 * @return The data
		 */
		public Object get(String field) {
			try {
				return object.getClass().getDeclaredField(field).get(object);
			} catch (IllegalArgumentException | IllegalAccessException
					| NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
				return null;
			}
		}
		
		/**
		 * Invoke a method
		 * @param method The method name
		 * @param args The arguments
		 * @return The reflection object
		 */
		public ReflectionObject invoke(String method, Object... args) {
			methods.put(method, args);
			
			return this;
		}
		
		/**
		 * Invokes a method now
		 * @param method The method name
		 * @param args The arguments
		 * @return The return of the method
		 */
		public Object invokeNow(String method, Object... args) {
			for(Method m : object.getClass().getDeclaredMethods()) {
				if(m.getName().equals(method)) {
					try {
						return m.invoke(object, args);
					} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return null;
		}
		
		/**
		 * Modifies the object
		 * @return The modified object
		 */
		public Object modify() {
			// Fields
			for(Field f : object.getClass().getDeclaredFields()) {
				f.setAccessible(true);
				if(fields.containsKey(f.getName())) {
					try {
						f.set(object, fields.get(f.getName()));
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			}
			
			// Methods
			for(Method m : object.getClass().getDeclaredMethods()) {
				m.setAccessible(true);
				if(methods.containsKey(m.getName())) {
					try {
						m.invoke(object, methods.get(m.getName()));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					} 
				}
			}
			
			return object;
		}
		
	}
}
