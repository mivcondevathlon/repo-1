package me.ilouhd.creepplays.devathlon.api;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.google.common.collect.ImmutableMap;

/**
 * 
 * @author CreepPlays
 *
 */
public class ParticleAPI {
	
	public enum Particle {
		
		EXPLODE("explode", 0),
		LARGE_EXPLOSION("largeexplosion", 1),
		HUGE_EXPLOSION("hugeexplosion", 2),
		FIREWORKS_SPARK("fireworksSpark", 3),
		BUBBLE("bubble", 4),
		SPLASH("splash", 5),
		WAKE("wake", 6),
		SUSPENDED("suspended", 7),
		DEATH_SUSPEND("deathsuspend", 8),
		CRIT("crit", 9),
		MAGIC_CRIT("magicCrit", 10),
		SMOKE("smoke", 11),
		LARGE_SMOKE("largeSmoke", 12),
		SPELL("spell", 13),
		INSTANT_SPELL("instantSpell", 14),
		MOB_SPELL("mobSpell", 15),
		MOB_SPELL_AMBIENT("mobSpellAmbient", 16),
		WITCH_MAGIC("witchMagic", 17),
		DRIP_WATER("dripWater", 18),
		DRIP_LAVA("dripLava", 19),
		ANGRY_VILLAGER("angryVillager", 20),
		HAPPY_VILLAGER("happyVillager", 21),
		TOWN_AURA("townaura", 22),
		NOTE("note", 23),
		PORTAL("portal", 24),
		ENCHANTMENT_TABLE("enchantmenttable", 25),
		FLAME("flame", 26),
		LAVA("lava", 27),
		FOOTSTEP("footstep", 28),
		CLOUD("cloud", 29),
		REDDUST("reddust", 30),
		SNOWBALL_POOF("snowballpoof", 31),
		SNOW_SHOWEL("snowshovel", 32),
		SLIME("slime", 33),
		HEART("heart", 34),
		BARRIER("barrier", 35),
		DROPLET("droplet", 39),
		TAKE("take", 40),
		MOB_APPEARANCE("mobappearance", 41);
		
		private final static Map<Integer, Particle> BY_ID;				
		private final static Map<String, Particle> BY_NAME;
		
		private final String name;
		private final int id;
		
		
		private Particle(String name, int id) {
			this.name = name;
			this.id = id;
		}
		
		private int getID() {
			return this.id;
		}
		
		@SuppressWarnings("unused")
		private String getName() {
			return this.name;
		}
		
		public static Particle getParticleByID(int id) {
			return BY_ID.get(id);
		}
		
		public static Particle getParticleByName(String name) {
			return BY_NAME.get(name);
		}
		
		static {
			ImmutableMap.Builder<Integer, Particle> byId = ImmutableMap.builder();
			ImmutableMap.Builder<String, Particle> byName = ImmutableMap.builder();
			
			for(Particle particle : values()) {
				byId.put(particle.id, particle);
				byName.put(particle.name, particle);
			}
			
			BY_ID = byId.build();
			BY_NAME = byName.build();
		}
	}
	
	public static void createParticle(Particle particle, Vector location, Vector offset, float speed, int amount) {
		Object packet;
		try {
			packet = ReflectionAPI.getNMSClass("PacketPlayOutWorldParticles").newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			return;
		}
		final Object modifiedPacket = new ReflectionAPI.ReflectionObject(packet)
					.set("a", particle.getID())
					.set("b", (float)location.getX())
					.set("c", (float)location.getY())
					.set("d", (float)location.getZ())
					.set("e", (float)offset.getX())
					.set("f", (float)offset.getY())
					.set("g", (float)offset.getZ())
					.set("h", speed)
					.set("i", amount)
					.set("j", false)
					.set("k", null)
					.modify();
		
		Bukkit.getOnlinePlayers().stream().forEach(player -> {
			sendPacket(player, modifiedPacket);
		});
	}
	
	private static void sendPacket(Player p, Object packet) {
		Object nmsPlayer = new ReflectionAPI.ReflectionObject(p).invokeNow("getHandle");
		Object playerConnection = new ReflectionAPI.ReflectionObject(nmsPlayer).get("playerConnection");
		new ReflectionAPI.ReflectionObject(playerConnection).invokeNow("sendPacket", packet);
	}
	
}
