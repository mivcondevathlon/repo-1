package me.ilouhd.creepplays.devathlon.api;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import me.ilouhd.creepplays.devathlon.Main;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class DisplayAPI {
	
	private List<Player> players = new ArrayList<Player>();
	
	private final Class<?> title = ReflectionAPI.getNMSClass("PacketPlayOutTitle");
	private final Class<?> enumtitleaction = ReflectionAPI.getNMSClass("PacketPlayOutTitle$EnumTitleAction");
	private final Class<?> chatserial = ReflectionAPI.getNMSClass("IChatBaseComponent$ChatSerializer");
	
	public DisplayAPI(Player player) {
		players.add(player);
	}
	
	public DisplayAPI(ArrayList<Player> players) {
		players.addAll(players);
	}
	
	public void setTablist(String headerText, String footerText) {
		if(players.size() == 0) {
			Bukkit.getConsoleSender().sendMessage(Main.getInstance().prefix + "�4Cannot set Tablist to unknown Players!");
			return;
		}
		
		Object craftChatMessage;
		try {
			craftChatMessage = ReflectionAPI.getCBClass("util.CraftChatMessage").newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			return;
		}
		
		Object header = Array.get(new ReflectionAPI.ReflectionObject(craftChatMessage).invokeNow("fromString", headerText), 0);
		Object footer = Array.get(new ReflectionAPI.ReflectionObject(craftChatMessage).invokeNow("fromString", footerText), 0);
		
		Object packet;
		try {
			packet = ReflectionAPI.getNMSClass("PacketPlayOutPlayerListHeaderFooter").newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			return;
		}
		
		final Object modifiedPacket = new ReflectionAPI.ReflectionObject(packet)
											.set("a", header)
											.set("b", footer)
											.modify();
		
		players.stream().forEach(player -> {
			sendPacket(player, modifiedPacket);
		});
	}
	
	public void sendTitles(String title, String subtitle, int fadeIn, int stay, int fadeOut) {
		players.stream().forEach(player -> {
			sendTimings(player, fadeIn, stay, fadeOut);
			sendTitle(player, title);
			sendSubTitle(player, subtitle);
		});
    }
		   
	public void sendTitle(String title, int fadeIn, int stay, int fadeOut) {
		players.stream().forEach(player -> {
			sendTimings(player, fadeIn, stay, fadeOut);
			sendTitle(player, title);
		});
	}
	
	public void sendTitle(Player p, String title) {
		players.stream().forEach(player -> {
			try {
				sendPacket(player, new ReflectionAPI.ReflectionObject(this.title.newInstance())
						.set("a", enumtitleaction.getDeclaredField("TITLE").get(null))
						.set("b", chatserial.getMethod("a", String.class).invoke(null, "{'text': '" + title + "'}"))
						.modify());
			} catch (InstantiationException | IllegalAccessException
					| IllegalArgumentException | NoSuchFieldException
					| SecurityException | InvocationTargetException
					| NoSuchMethodException e) {
				e.printStackTrace();
			}
		});
	}
	
	public void sendSubTitle(String subtitle, int fadeIn, int stay, int fadeOut) {
		players.stream().forEach(player -> {
			sendTimings(player, fadeIn, stay, fadeOut);
			sendSubTitle(player, subtitle);
		});
	}
		   
	public void sendSubTitle(Player p, String subtitle) {
		players.stream().forEach(player -> {
			try {
				sendPacket(p, new ReflectionAPI.ReflectionObject(this.title.newInstance())
					.set("a", enumtitleaction.getDeclaredField("SUBTITLE").get(null))
					.set("b", chatserial.getMethod("a", String.class).invoke(null, "{'text': '" + subtitle + "'}"))
					.modify());
			} catch (InstantiationException | IllegalAccessException
					| IllegalArgumentException | NoSuchFieldException
					| SecurityException | InvocationTargetException
					| NoSuchMethodException e) {
				e.printStackTrace();
			}
		});
	}
		   
	private void sendTimings(Player p, int fadeIn, int stay, int fadeOut) {
		try {
			sendPacket(p, new ReflectionAPI.ReflectionObject(title.newInstance())
					.set("a", enumtitleaction.getDeclaredField("TIMES").get(null))
					.set("c", fadeIn)
					.set("d", stay)
					.set("e", fadeOut)
					.modify());
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | NoSuchFieldException
				| SecurityException e) {
			e.printStackTrace();
		}
   }
		   
   public void resetTitle(Player p) {
	   try {
		   sendPacket(p, new ReflectionAPI.ReflectionObject(title.newInstance())
					.set("a", enumtitleaction.getDeclaredField("RESET").get(null))
			   		.modify());
	   } catch (InstantiationException | IllegalAccessException
			   | IllegalArgumentException | NoSuchFieldException
			   | SecurityException e) {
				e.printStackTrace();
		}
   }
		   
   public void clearTitle(Player p) {
	   try {
		   sendPacket(p, new ReflectionAPI.ReflectionObject(title.newInstance())
		   				.set("a", enumtitleaction.getDeclaredField("CLEAR").get(null))
						.modify());
	   } catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | NoSuchFieldException
				| SecurityException e) {
			e.printStackTrace();
		}
   }
   
   private void sendPacket(Player p, Object packet) {
		Object nmsPlayer = new ReflectionAPI.ReflectionObject(p).invokeNow("getHandle");
		Object playerConnection = new ReflectionAPI.ReflectionObject(nmsPlayer).get("playerConnection");
		new ReflectionAPI.ReflectionObject(playerConnection).invokeNow("sendPacket", packet);
	}
   
}
