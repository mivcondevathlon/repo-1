package me.ilouhd.creepplays.devathlon.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import me.ilouhd.creepplays.devathlon.Main;

import org.bukkit.Bukkit;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

/**
 * 
 * @author ilouHD
 *
 */
public class MySQL {
	
	private ExecutorService executor;
	
	private String host, username, password, database;
	private Integer port;
	
	private Connection connection;
	public Connection getConnection() { return connection; }
	public boolean hasConnection() {
		return connection != null;
	}
	
	private Gson gson;
	private Main main;
	
	private File dataFolder = new File("plugins/Mivcon");
	private File configFile = new File(dataFolder, "config.json");
	
	@SuppressWarnings({ "unchecked"})
	public MySQL() {
		gson = new GsonBuilder().setPrettyPrinting().create();
		main = Main.getInstance();
		
		if(!dataFolder.exists()) {
			dataFolder.mkdirs();
		}
		if(!configFile.exists()) {
			try {				
				configFile.createNewFile();
				
				JSONObject json = new JSONObject();
				
				json.put("host", "localhost");
				json.put("port", 3306);
				json.put("username", "root");
				json.put("password", "password");
				json.put("database", "database");
				
				FileWriter writer = new FileWriter(configFile);
				writer.write(gson.toJson(json));
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			FileInputStream input = new FileInputStream(configFile);
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			
			JsonObject json = gson.fromJson(reader, JsonObject.class);
				
			this.host = json.get("host").getAsString();
			this.port = json.get("port").getAsInt();
			this.username = json.get("username").getAsString();
			this.password = json.get("password").getAsString();
			this.database = json.get("database").getAsString();
			
			reader.close();
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.executor = Executors.newCachedThreadPool();
		
		connectToDatabase();
	}

	public void connectToDatabase() {
		try {
			connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?autoReconnect=true", this.username, this.password);
			Bukkit.getConsoleSender().sendMessage(main.prefix + "�2Successfully connected to database " + this.database + "@" + this.host + ":" + this.port + "!");
		} catch (SQLException e) {
			e.printStackTrace();
			Bukkit.getConsoleSender().sendMessage(main.prefix + "�4Cannot connect to database " + this.database + "@" + this.host + ":" + this.port + "!");
		}
	}
	
	public void closeConnection() {
		if(hasConnection()) {
			try {
				connection.close();
				Bukkit.getConsoleSender().sendMessage(main.prefix + "�2Successully disconnected from MySQL-Database!");
			} catch(SQLException e) {
				Bukkit.getConsoleSender().sendMessage(main.prefix + "�4Could not disconnect from MySQL-Database (SQLException)!");
			}
		} else {
			Bukkit.getConsoleSender().sendMessage(main.prefix + "�4Could not disconnect from MySQL-Database (No Connection)!");
		}
	}
	
	public void update(String query) {
		executor.execute(new Runnable() {
			
			@Override
			public void run() {
				try {
					PreparedStatement pStatement = connection.prepareStatement(query);
					pStatement.executeUpdate();
					pStatement.close();
				} catch (SQLException e) {
					connectToDatabase();
					Bukkit.getConsoleSender().sendMessage(main.prefix + "�4Could not prepare Statement (Update)!");
				}
			}
		});
	}
	
	private class QueryCallable implements Callable<ResultSet> {
		private PreparedStatement preparedStatement;
		public QueryCallable(PreparedStatement preparedStatement) {
			this.preparedStatement = preparedStatement;
		}
		public ResultSet call() {
			ResultSet resultSet = null;
			try {
				resultSet = preparedStatement.executeQuery();
				preparedStatement.close();
			} catch (SQLException e) {
				connectToDatabase();
				Bukkit.getConsoleSender().sendMessage(main.prefix + "�4Could not prepare Statement (Query)!");
			}
			return resultSet;
		}
	}
	
	public ResultSet query(PreparedStatement query) {
		QueryCallable callable = new QueryCallable(query);
		Future<ResultSet> future = executor.submit(callable);
		try {
			return future.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
