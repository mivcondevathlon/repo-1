package me.ilouhd.creepplays.devathlon.api;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;

/**
 * 
 * @author Alex
 *
 */
public class WorldManager {

	// TODO Testen
	public static void resetWorld(World world) {
		
		for(Player p : world.getPlayers()) {
			p.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
		}
		
		Bukkit.unloadWorld(world, false);
		WorldCreator.name(world.getName()).copy(world).createWorld();
		
	}
	
}
