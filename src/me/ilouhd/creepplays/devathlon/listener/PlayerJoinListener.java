package me.ilouhd.creepplays.devathlon.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		e.getPlayer().setMaxHealth(4.0);
		e.getPlayer().getInventory().setArmorContents(null);
		e.getPlayer().getInventory().clear();
	}
}
