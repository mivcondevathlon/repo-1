package me.ilouhd.creepplays.devathlon.listener;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

/**
 * @author Alex
 */
public class EntityDeathListener implements Listener {

	@EventHandler
	public void onDeath(EntityDeathEvent e) {
		if(e.getEntity().getKiller() == null) return;
		double newHearts;
		if(e.getEntityType().equals(EntityType.PLAYER)) {
			newHearts = e.getEntity().getKiller().getMaxHealth() + 4.0;
		} else {
			newHearts = e.getEntity().getKiller().getMaxHealth() + 2.0;
		}
		e.getEntity().getKiller().setMaxHealth(newHearts);
		e.getEntity().getKiller().setHealth(newHearts);
	}
	
}
