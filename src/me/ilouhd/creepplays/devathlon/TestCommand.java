package me.ilouhd.creepplays.devathlon;

import me.ilouhd.creepplays.devathlon.api.DisplayAPI;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TestCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(command.getName().equalsIgnoreCase("test")) {
			if(!(sender instanceof Player)) {
				sender.sendMessage(Main.getInstance().prefix + "�cYou need to be a player to perform this command!");
				return true;
			}
			Player player = (Player) sender;
			
			if(args.length != 1) {
				player.sendMessage(Main.getInstance().prefix + "�cThere's an error in your command-syntax!");
				return true;
			}
			
			if(args[0].equalsIgnoreCase("title")) {
				DisplayAPI display = new DisplayAPI(player);
				display.sendTitles("Test", "Test-Title", 10, 100, 10);
			} else if(args[0].equalsIgnoreCase("updateTablist")) {
				DisplayAPI display = new DisplayAPI(player);
				display.setTablist("Uberschrift", "Unterschrift");
			}
			
		}
		return true;
	}

}
